<?php

class Sql extends PDO {

    private $conn;

    public function __construct()
    {
        // $this->conn = new PDO("mysql:host=localhost;dbname=speedfix", "root", "");
        $this->conn = new PDO("mysql:host=localhost;dbname=cms-popus", "root", "");
    }

    private function setparams($statement, $parameters = array())
    {
        foreach ($parameters as $key => $value) {
            $this->setParam($statement, $key, $value);
        }
    }

    private function setParam($statement, $key, $value)
    {
        $statement->bindParam($key, $value);
    }

    public function query($rawQuery, $params = array())
    {
        $stmt = $this->conn->prepare($rawQuery);
        $this->setparams($stmt, $params);
        $stmt->execute();

        return $stmt;
    }

    public function select($rawQuery, $params = array())
	{
		$stmt = $this->query($rawQuery, $params);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

}

?>
