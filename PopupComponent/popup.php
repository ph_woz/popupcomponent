<?php 

    require 'PopupComponent/config.php';
    $empresa = Empresa::find('exampledomain.com');
    $empresa = $empresa[0]; 

    if($empresa != null) { 
?>
    <script> $(document).ready(function() { $("#modalPopup").modal('show'); }); </script>
    <div class="modal fade" id="modalPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bg-transparent border-0">

            <div class="d-flex justify-content-end">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true" class="text-white display-4">&times;</span>
              </button>
            </div>

            <img src="https://popups.dmxapp.com.br/popups/<?= $empresa['id']; ?>/<?= $empresa['popup']; ?>" class="img-fluid">

        </div>
      </div>
    </div>

<?php } ?>