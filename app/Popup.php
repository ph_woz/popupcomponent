<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{

    public $timestamps = true;
    
	protected $fillable = [
		'site','popup','de','ate','status','cadastrante'
	];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model){
            $model->cadastrante = Auth::user()->name ?? 'Não identificado'; 
        });
    }

}
