<?php

namespace App\Http\Controllers;

use App\Popup;
use Illuminate\Http\Request;

class PopupController extends Controller
{

    public function popups() {
        
        $popups = Popup::orderBy('id','desc')->get();

        return view('popups', compact('popups') );
    }

    public function addpopup(Request $request) {
        
        if($request->has('site','de','ate')) {

            $verifyIfExist = Popup::where('site', $request->input('site'))->count();

            if($verifyIfExist > 0) {

                return back()
                     ->withInput()
                     ->with('danger','Já tem este site cadastrado no banco de dados.');
            }

            $doAno = $request->input('doAno');
            $ateAno = $request->input('ateAno');

            $doAno = '/' . $doAno;
            $ateAno = '/' . $ateAno;

            $de = explode(' ', $request->input('de'));
            $de = $de[0] . $doAno . ' ' . $de[1];

            $ate = explode(' ', $request->input('ate'));
            $ate = $ate[0] . $ateAno . ' ' . $ate[1];

            $de = str_replace('/', '-', $de);
            $de = date('Y-m-d H:i', strtotime($de));

            $ate = str_replace('/', '-', $ate);
            $ate = date('Y-m-d H:i', strtotime($ate));

            $popup = $request->file('file');
            $filename = $popup->getClientOriginalName();

            $request->merge([
                'de' => $de,
                'ate' => $ate,
                'popup' => $filename
            ]);

        	$popupCreated = Popup::create($request->all());

            $popup->move('popups/'.$popupCreated->id.'/', $filename);

        	return back()->with('success','Popup adicionado com êxito.');
        }

        return view('addpopup');
    }

    public function settings($id, Request $request) {
        
        $popup = Popup::find($id);

        $delete_popup = $request->input('delete_popup');

        if($delete_popup != null) {

            if(file_exists('popups/'.$popup->id.'/'.$popup->popup)) {
            
                unlink('popups/'.$popup->id.'/'.$popup->popup);

            }

            $popup->update(['popup' => null]);
        }


        if($request->has('site','de','ate')) {

            $verifyIfExist = Popup::where('site', $request->input('site'))
            ->where('id','<>', $popup->id)
            ->count();

            if($verifyIfExist > 0) {

                return back()
                     ->withInput()
                     ->with('danger','Já tem este site cadastrado no banco de dados.');
            }

            $popupFile = $request->file('file');
            
            if($popupFile != null) {
            
                $filename = $popupFile->getClientOriginalName();
                $popupFile->move('popups/'.$popup->id.'/', $filename);
                $request->merge(['popup' => $filename]);
            }

            $doAno = $request->input('doAno');
            $ateAno = $request->input('ateAno');

            $doAno = '/' . $doAno;
            $ateAno = '/' . $ateAno;

            $de = explode(' ', $request->input('de'));
            $de = $de[0] . $doAno . ' ' . $de[1];

            $ate = explode(' ', $request->input('ate'));
            $ate = $ate[0] . $ateAno . ' ' . $ate[1];

            $de = str_replace('/', '-', $de);
            $de = date('Y-m-d H:i', strtotime($de));
            
            $ate = str_replace('/', '-', $ate);
            $ate = date('Y-m-d H:i', strtotime($ate));

            $request->merge([
                'de' => $de,
                'ate' => $ate
            ]);

            Popup::find($id)->update($request->all());

            return back()->with('success','Popup atualizado com êxito.');
        }

        return view('settings', compact('popup') );
    }


    public function delete(Request $request) {

        $popup = Popup::find($request->input('delete_id'));

        if(file_exists('popups/'.$popup->id.'/'.$popup->popup)) {
        
            unlink('popups/'.$popup->id.'/'.$popup->popup);

        }

        Popup::destroy($popup->id);

        return back()->with('success','Popup deletado com êxito.');
    }

}
