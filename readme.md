# DMX WEB POPUPS

### Requisitos

* Bootstrap 4;

-------------------

### Implementação

* Coloque a pasta "PopupComponent" no mesmo lugar onde fica o arquivo "index.php";

* Abra o arquivo "popup.php" e coloque o site da empresa que está cadastrado no banco de dados;

* Fique livre para importar o component na página que quiser. Exemplo no diretório da página index.php : "<?php include 'PopupComponent/popup.php'; ?>"       