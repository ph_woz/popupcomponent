@extends('layouts.app')
@section('content')

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">@csrf
</form>

<div class="container pt-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="px-3 py-2 font-nunito weight-700 ls-05 border-bottom bg-base text-white">    <div class="d-flex justify-content-between">

                        <span>Dashboard</span>

                        <a href="#" class="bg-transparent text-white hover-d-none" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <span class="ml-3">
                                <i class="fa fa-arrow-left fs-15"></i>
                                Sair
                            </span>
                        </a>

                    </div>
                </div>

                <div class="card-body">

                    <p class="my-2 font-nunito">Olá, {{ Auth::user()->name }}.</p>
                    <p class="font-nunito">Bem vindo a DMXWEB Popups.</p>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
