@extends('layouts.app')
@section('content')
<div class="container pt-4">

    @if(session('success'))
        <div class="alert alert-success">
            <b>Sucesso!</b> {{ session('success') }}
        </div>
    @endif
    @if(session('danger'))
        <div class="alert alert-danger">
            <b>Ops!</b> {{ session('danger') }}
        </div>
    @endif

    <form method="POST" enctype="multipart/form-data">
    @csrf

        <div class="d-md-flex mb-4">

            <input type="text" name="site" class="form-control p-4 w-md-30 w-xs-100" placeholder="Website" required>

            <input type="file" name="file" class="mx-md-3 my-3 my-md-0 border rounded py-2 px-3 w-xs-100 w-md-25" required>


            <input type="text" name="doAno" maxlength="4" class="rounded-right-0 rounded-left border p-2 w-md-7" placeholder="Do Ano de" required value="{{ date('Y') }}">

            <input type="text" name="de" class="rounded-left-0 rounded-right datetime border p-2 w-md-13" placeholder="De" required value="{{ date('d/m H:i') }}">

            <div class="mx-md-2"></div>

            <input type="text" name="ateAno" maxlength="4" class="rounded-right-0 rounded-left border p-2 w-md-7" placeholder="Até o Ano de" required value="{{ date('Y') }}">

            <input type="text" name="ate" class="rounded-left-0 rounded-right datetime border p-2 w-md-13" placeholder="Até" required>

            <div class="mx-md-2"></div>

            <select name="status" class="border rounded w-md-25 w-xs-100 p-2" required>
                <option value="0" selected>Ativo</option>
                <option value="1">Inativo</option>
            </select>

        </div>

        <button type="submit" class="cursor-pointer bg-base rounded border-0 px-3 py-2 text-light font-nunito weight-700 ls-05">
            Cadastrar
        </button>

    </form>

</div>


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>

    $(".datetime").mask('00/00 00:00');

</script>
@endpush


@endsection
