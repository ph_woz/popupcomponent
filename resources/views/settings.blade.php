@extends('layouts.app')
@section('content')
<div class="container pt-4">

    @if(session('success'))
        <div class="alert alert-success">
            <b>Sucesso!</b> {{ session('success') }}
        </div>
    @endif
    @if(session('danger'))
        <div class="alert alert-danger">
            <b>Ops!</b> {{ session('danger') }}
        </div>
    @endif

    <form method="POST" enctype="multipart/form-data">
    @csrf

        <div class="d-md-flex mb-4-5">

            <input type="text" name="site" class="form-control p-4 w-md-30 w-xs-100" placeholder="Website" required value="{{ $popup->site }}">

            <input type="file" name="file" class="mx-md-3 my-3 my-md-0 border rounded py-2 px-3 w-xs-100 w-md-25">


            <input type="text" name="doAno" maxlength="4" class="rounded-right-0 rounded-left border p-2 w-md-7" placeholder="Do Ano de" required value="{{ date('Y', strtotime($popup->de) ) }}">

            <input type="text" name="de" class="rounded-left-0 rounded-right datetime border p-2 w-md-13" placeholder="De" required value="{{ date('d/m H:i', strtotime($popup->de)) }}">

            <div class="mx-md-2"></div>

            <input type="text" name="ateAno" maxlength="4" class="rounded-right-0 rounded-left border p-2 w-md-7" placeholder="Até o Ano de" required value="{{ date('Y', strtotime($popup->ate) ) }}">

            <input type="text" name="ate" class="rounded-left-0 rounded-right datetime border p-2 w-md-13" placeholder="Até" required value="{{ date('d/m H:i', strtotime($popup->ate)) }}">

            <div class="mx-md-2"></div>

            <select name="status" class="border rounded w-md-25 w-xs-100 p-2" required>
                <option value="0" @if($popup->status == 0) selected @endif>Ativo</option>
                <option value="1" @if($popup->status == 1) selected @endif>Inativo</option>
            </select>

        </div>

        <input type="hidden" name="delete_popup" id="delete_popup">

        @if($popup->popup != null)
        <div id="boxImgPopup">
            <div class="d-flex align-items-start">
                <img src="{{asset('popups')}}/{{$popup->id}}/{{$popup->popup}}" class="mb-4-5 img-fluid border" width="150" id="imgPopup">
                <button type="button" class="rounded-circle cursor-pointer btn btn-danger" onclick="
                    document.getElementById('delete_popup').value = 'ok';
                    document.getElementById('boxImgPopup').style.display = 'none';
                " style="margin-top: -13px;margin-left: -22px;">
                    <i class="fa fa-trash"></i>
                </button>
            </div>
        </div>
        @endif

        <button type="submit" class="cursor-pointer bg-base rounded border-0 px-3 py-2 text-light font-nunito weight-700 ls-05">
            Cadastrar
        </button>

    </form>

</div>

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>

    $(".datetime").mask('00/00 00:00');

</script>
@endpush

@endsection
