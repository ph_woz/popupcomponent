@extends('layouts.app')
@section('content')
<div class="container pt-4">

	<div class="table-responsive">
		<table class="table table-striped">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Site</th>
		      <th scope="col">De</th>
		      <th scope="col">Até</th>
		      <th scope="col">Status</th>
		      <th scope="col">Cadastrante</th>
		      <th scope="col">Ações</th>
		    </tr>
		  </thead>
		  <tbody>

		  	@foreach($popups as $popup)
		    <tr>

		      <td>{{ $popup->id }}</td>

		      <td>
		      	<a href="http://{{$popup->site}}" target="_blank" class="text-base font-nunito weight-600">
		      		{{ $popup->site }}
		      	</a>
		      </td>
		      
		      <td>{{ date('d/m/Y H:i', strtotime($popup->de)) }}</td>
		      <td>{{ date('d/m/Y H:i', strtotime($popup->ate)) }}</td>

		      <td>
		      	@if($popup->status == 0)
		      		Ativo
		      	@else
		      		Desativado
		      	@endif
		      </td>

		      <td>{{ $popup->cadastrante }}</td>
		      
		      <td>
	
		      	<a href="{{url('popup/settings')}}/{{$popup->id}}" class="my-2 my-md-0 btn btn-warning">
		      		<i class="fa fa-edit"></i>
		      	</a>	

		      	<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#ModalDelete"
		      	   onclick="document.getElementById('delete_id').value='{{$popup->id}}'">
		      		<i class="fa fa-trash"></i>
		      	</a>	

		      </td>

		    </tr>
		    @endforeach

		  </tbody>
		</table>
	</div>

</div>

<form method="POST" action="delete">
@csrf
	
	<input type="hidden" name="delete_id" id="delete_id">

	<div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalCenterTitle">Deletar Popup</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body flex-center">
	        <p class="font-nunito weight-600 ls-05 fs-17 mt-3">Você tem certeza que deseja deletar este Popup?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="cursor-pointer bg-base rounded border-0 px-3 py-2 text-light font-nunito weight-700 ls-05 fs-15">
            	Confirmar
        	</button>
	      </div>
	    </div>
	  </div>
	</div>

</form>

@endsection