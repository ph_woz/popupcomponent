@extends('layouts.app')
@section('content')

<style> body { background: #343a40; } </style>

<div class="h-100vh bg-dark" style="margin-top: -100px;">
    <div class="container h-100">
        <div class="row align-items-center h-100">
            <div class="col-md-6 mx-auto">
                <div class="h-100 justify-content-center shadow">
                    <div class="bg-white p-5 rounded-1">
                        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">

                            @csrf

                            <p class="mb-0 font-nunito fs-23 text-center">
                                DMXWEB Popups
                            </p>
                            <hr class="mt-0 mb-4-5" />

                            <div class="input-group mb-2">

                                <div class="input-group-prepend">
                                  <div class="bg-f1 input-group-text">
                                    <i class="fa fa-user"></i>
                                  </div>
                                </div>

                                <input id="name" type="text" class="bg-f1 p-4 form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required placeholder="Nome" autofocus>   

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback mb-3" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="input-group mb-2">

                                <div class="input-group-prepend">
                                  <div class="bg-f1 input-group-text">
                                    <i class="fa fa-envelope"></i>
                                  </div>
                                </div>

                                <input id="email" type="email" class="bg-f1 p-4 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">   

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback mb-3" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="input-group">

                                <div class="input-group-prepend">
                                  <div class="bg-f1  input-group-text">
                                    <i class="fa fa-key"></i>
                                  </div>
                                </div>

                                <input id="password" type="password" class="bg-f1 p-4 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Senha">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div align="center" class="mt-4-5">
                                <button type="submit" class="cursor-pointer bg-base py-2 px-4 border-0 rounded text-light font-nunito ls-05 weight-700">
                                    Cadastrar
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div> 
        </div>  
    </div> 
</div>

@endsection
