<nav class="navbar navbar-expand-lg navbar-light bg-base-opacity fixed-top shadow">
  <div class="container">
    <a class="navbar-brand fs-16 weight-600 text-white font-raleway ls-2 text-shadow scroll-link" href="{{url('dashboard')}}">
      DMXWEB Popups
    </a>
    <button class="navbar-toggler outline-0 border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fa fa-bars text-white"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">

        <li class="nav-item">
          <a class="nav-link scroll-link" href="{{ url('popup/popups') }}">Popups</a>
        </li>

        <div class="line-vertical"></div>

        <li class="nav-item">
          <a class="nav-link scroll-link" href="{{ url('popup/addpopup') }}">Cadastrar</a>
        </li>

      </ul>
    </div>
  </div>
</nav>




