<?php

use Illuminate\Database\Seeder;	
use Illuminate\Support\Facades\Hash;
use App\User;


class UsersTableSeeder extends Seeder
{
	public function run()
	{

          User::create([
             'name' => 'Pedro Henrique',
             'email' => 'pedrohenrique1207ph@gmail.com',
             'password' => Hash::make('26533955'),
             'status' => 0
          ]);

	}

}
